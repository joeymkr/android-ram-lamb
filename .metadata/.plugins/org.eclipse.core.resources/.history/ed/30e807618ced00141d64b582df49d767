package com.emag.Emag;

import com.example.Emag.gameframework.Game;
import com.example.Emag.gameframework.Graphics;
import com.example.Emag.gameframework.Input;
import com.example.Emag.gameframework.Screen;

import java.util.ConcurrentModificationException;
import java.util.List;

/**
 * Created by Joe Roberts on 24/04/2015.
 * Type of screen which displays menu image and listens for and deals with menu screen touch events. The touch events
 * are iterated through in a synchronized block to determine whether a set of specific areas of the screen have been
 * touched. The synchronization is so that the iterated data (list of touchEvents) is not modified during iteration which
 * can cause a Concurrent Modification Exception.
 */
public class MenuScreen extends Screen{

    public MenuScreen(Game game){
        super(game);
    }

    /**
     * Method which is called from AndroidFastRenderView using a timer. This method listens for and checks all touch-
     * events in a synchronized block. This is so the data (List<touchEvents>) is not modified during iteration which
     * can cause a Concurrent Modification Exception.
     * @param deltaTime
     */
    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();

        try{
        synchronized (touchEvents){
        for (Input.TouchEvent event : touchEvents){
            if (event.type == Input.TouchEvent.TOUCH_DOWN) {
                if(event.x < 150 && event.y > 250){
                    game.setScreen(new GameScreen(game));
                } else if(event.x < 150 && event.y > 200 && event.y < 250){
                    game.setScreen(new HelpScreen1(game));
                }
            }
        }}}catch (ConcurrentModificationException cme){

        }
    }

    /**
     * Draws menu image to screen
     * @param deltaTime
     */
    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(GraphicAssets.menu, 0, 0);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
