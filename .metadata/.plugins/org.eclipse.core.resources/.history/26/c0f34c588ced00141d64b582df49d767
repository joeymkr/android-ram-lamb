package com.example.Emag;

import com.example.Emag.gameframework.Game;
import com.example.Emag.gameframework.Graphics;
import com.example.Emag.gameframework.Input;
import com.example.Emag.gameframework.Screen;

import java.util.List;

/**
 * Created by Joe Roberts on 25/04/2015.
 * Type of screen which displays the main game and listens for and deals with the game touch events. The touch events
 * are iterated through in a synchronized block to determine whether a set of specific areas of the screen have been
 * touched. The synchronization is so that the iterated data (list of touchEvents) is not modified during iteration which
 * can cause a Concurrent Modification Exception.
 *
 * Here we take user input and tell the characters and the environment, the direction to move in or flags to set such
 * as jumping or attacking. Essentially this is the middle man which turns the user action into a message then distributed
 * to the specific objects. The result of these actions are not dealt with here. This class does however, determine the
 * images to draw based on each obstacles state. The main purpose is to update the graphics of the current games state,
 * the logic of the game is dealt with both by each individual obstacle and the Environment.UpdateEnvironment() method.
 */
public class GameScreen extends Screen {

    private boolean leftFlag = false;
    private boolean rightFlag = false;
    private boolean played = false;


    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver,
        LevelComplete
    }
    GameState state = GameState.Running;

    private Environment environment;

    /**
     * Constructor that takes a Game and initialises a default level of 1
     * @param game
     */
    public GameScreen(Game game){
        this(game, 3);
    }

    /**
     * Constructor which takes a Game for the super and instaniates the environment and gives it a starting level.
     * @param game
     * @param level
     */
    public GameScreen(Game game, int level){

        super(game);
        environment = new Environment(this, level);
    }

    /**
     * Method which is called from AndroidFastRenderView using a timer. Determines the game state by using the
     * GameState enum variable. Calls updateRunningGame is running is the game state, gameOverUI is the state is
     * gameover and completedUI if the state is levelcompleted.
     * @param deltaTime
     */
    @Override
    public void update(float deltaTime) {

        Graphics g = game.getGraphics();
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();

        if(state == GameState.Running){
            updateRunningGame(touchEvents);
        } else if (state == GameState.GameOver){
            gameOverUI(touchEvents);
        } else if (state == GameState.LevelComplete){
            completedUI(touchEvents);
        }
    }

    /**
     * restarts the gameScreen by resetting all flags and default variables disposes of the audio files to re-instantiate
     * them along with the environment.
     * @param level due to this method being available from several sources the desired level is needed to be used to
     *              determine the environment settings.
     */
    private void restartGame(int level){
        played = false;
        GraphicAssets.won.dispose();
        GraphicAssets.won = game.getAudio().newMusic("won.mp3");
        rightFlag = false;
        leftFlag = false;
        if (level > 3){level = 1;}
        environment = new Environment(this, level);
        state = GameState.Running;
    }

    /**
     * sets the state of this game using GameState
     * @param state
     */
    public void setGameState(GameState state){
        this.state = state;
    }

    /**Listens for and deals with game touch events. The touch events are iterated through in a synchronized block to
     * determine whether a set of specific areas of the screen have been touched. The synchronization is so that the
     * iterated data (list of touchEvents) is not modified during iteration which can cause a Concurrent Modification
     * Exception.
     *
     * Sets movement flags for both the player and environment based on user input as well as jumping flags along with
     * a new thread to animate the jump. Here we also call the player.Attack() to run. On TOUCH_DOWN the flags are set,
     * on TOUCH_UP the flags are reset to defaults.
     *
     * Here we also call the update character methods and update the environment before drawing to screen.
    */
    private void updateRunningGame(List<Input.TouchEvent> touchEvents){

        if (!environment.player.isCharAlive()){
            state = GameState.GameOver;
        }
        synchronized (touchEvents){
        for (Input.TouchEvent event : touchEvents){
            if (event.type == Input.TouchEvent.TOUCH_DOWN){

                // move left button pressed
                if (event.x > 0 && event.x < 70){
                    rightFlag = false;
                    leftFlag = true;
                    environment.player.setFacingDir(Direction.LEFT);
                // move right button pressed
                } else if(event.x > 70 && event.x < 140){
                    leftFlag = false;
                    rightFlag = true;
                    environment.player.setFacingDir(Direction.RIGHT);
                // jump button pressed
                } else if(event.x > 400 && event.y < 150){
                    Thread jumpThread = new Thread(environment.player);
                    jumpThread.start();
                }
                else if(event.x > 400 && event.y > 150){
                    environment.player.attack();
                }
            //buttons released stops movement
            } else if (event.type == Input.TouchEvent.TOUCH_UP){

                if (event.x > 0 && event.x < 200){
                    rightFlag = false;
                    leftFlag = false;
                }
            }
        }}

        //moves character and environment based on dir flag
        if (rightFlag && environment.target.getX() > 10){
            environment.player.moveCharacterRight(1);
            environment.player.animateCharacter();
            environment.moveEnviroLeft();
        }
        //moves character and environment based on dir flag
        if (leftFlag && !(environment.player.getX() <= 5)){
            environment.player.moveCharacterLeft(1);
            environment.player.animateCharacter();
            environment.moveEnviroRight();
        }
        for (Wolf wolf : environment.wolves){
            wolf.updateCharacter();
        }
        environment.player.updateCharacter();
        environment.gravity();// applies gravity
        environment.updateEnvironment();
    }

    /**
     * Draws game components to screen based on each obstacles state and draws background and ground as standard. Here
     * we uses a calculation to continuously draw the background seamlessly along with the ground. The method only ever
     * draws two of each at most, when an image's x coordinate reaches (0 - the_images_width) it's position is reset to 0,
     * with the second image filling in the gap before the reset.
     * @param deltaTime
     */
    @Override
    public void present(float deltaTime) {

        if (state != GameState.Running){
            return;
        }
        Graphics g = game.getGraphics();

        //continuous background
        g.drawPixmap(GraphicAssets.background[environment.levelNumber-1],environment.getBkX(),environment.getBkY());
        if(environment.getBkX() < (0-(601 - 480))){
            g.drawPixmap(GraphicAssets.background[environment.levelNumber-1],environment.getBkX()+601,environment.getBkY());
        }else if (environment.getBkX() > 0){
            g.drawPixmap(GraphicAssets.background[environment.levelNumber-1],environment.getBkX()-601,environment.getBkY());
        }
        //continuous ground
        g.drawPixmap(GraphicAssets.ground[environment.levelNumber-1],environment.getGroundX(),environment.getGroundY());
        if(environment.getGroundX() < (0-(601 - 480))){
            g.drawPixmap(GraphicAssets.ground[environment.levelNumber-1],environment.getGroundX()+601,environment.getGroundY());
        }else if (environment.getGroundX() > 0){
            g.drawPixmap(GraphicAssets.ground[environment.levelNumber-1],environment.getGroundX()-601,environment.getGroundY());
        }
        if(environment.getGroundX() > 0){
            g.drawPixmap(GraphicAssets.ground[environment.levelNumber-1],environment.getGroundX()-601,environment.getGroundY());
        }

        drawEnvironment();
        drawInfo();
    }

    /**
     * Draws the information bar across the top of the gameScreen showing player life and current level. To draw the
     * numbers a sprite sheet is uses containing numbers 0 - 9. Depending on the value of the current players life,
     * the each digit is multiplied by 20 (the width of each number in the sprite sheet) to locate the correct number in
     * the sprites.
     */
    public void drawInfo(){
        Graphics g = game.getGraphics();
        g.drawPixmap(GraphicAssets.info,0,0);

        String life = String.valueOf(environment.player.getLife());
        int length = life.length();
        int x = 130;
        for (int i = 0; i < length; i++){
            char character = life.charAt(i);

            int srcX = ((character - '0') * 19);

            g.drawPixmap(GraphicAssets.numbers,x,0, srcX, 0, 19, 25);
            x += 25;
        }

        String level = String.valueOf(environment.levelNumber);
        char character = level.charAt(0);
        int srcX = ((character - '0') * 19);
        g.drawPixmap(GraphicAssets.numbers,405,0, srcX, 0, 19, 25);

    }

    /**
     * Here we draw each component of the environments and determine the images of characters depending on their
     * current state. The components to be drawn include each platform, each wolf, the target and the player. Only the
     * components within the bounds of the visible screen are drawn.
     */
    public void drawEnvironment(){
        Graphics g = game.getGraphics();

        int playerX = environment.player.getX();
        int playerY = environment.player.getY();

        //loops through platforms and draws each that is within the screen area
        for (Platform platform: environment.platforms){

            //checks if platform position is within screen boundaries to draw it
            if (platform.getX() < 480 && platform.getX() > -200){
                g.drawPixmap(GraphicAssets.ground[environment.levelNumber-1],platform.getX(),platform.getY(), 0, 0, 200, platform.getHeight());
            }
        }

        //loops through platforms and draws each that is within the screen area
        for (Wolf wolf: environment.wolves){

            //checks if platform position is within screen boundaries to draw it
            if (wolf.getX() < 480 && wolf.getX() > -50 && wolf.isCharAlive()){
                g.drawPixmap(GraphicAssets.wolfImage[wolf.getSpriteNumber()],wolf.getX(),wolf.getY());
            }
        }

        //target
        if (environment.target.getX() < 480 && environment.target.getX() > -50){
            g.drawPixmap(GraphicAssets.target,environment.target.getX(),environment.target.getY());
        }

        //draws player
        //checks if player is hurt
        if(environment.player.isHurt()){
            if ((environment.player.getHurtTimer()%10) == 0){
                //flickers between blank image and sheep image to show player hurt
                g.drawPixmap(GraphicAssets.blank,playerX,playerY );
            } else{
                g.drawPixmap(GraphicAssets.sheepImage[environment.player.getSpriteNumber()],playerX,playerY );
            }
        }else if(environment.player.isAttacking()) {
            if (environment.player.getFacingDirection() == Direction.RIGHT){
                g.drawPixmap(GraphicAssets.sheepImage[6], playerX, playerY);
            }else {
                g.drawPixmap(GraphicAssets.sheepImage[7], playerX, playerY);
            }
        }else{
            g.drawPixmap(GraphicAssets.sheepImage[environment.player.getSpriteNumber()], playerX, playerY);
        }

    }

    /**
     * Method which is called when the GameState is gameOver. This method listens for and checks all touch-
     * events in a synchronized block. This is so the data (List<touchEvents>) is not modified during iteration which
     * can cause a Concurrent Modification Exception. This method also displays the levelcompleted image.
     */
    private void gameOverUI(List<Input.TouchEvent> touchEvents){
        Graphics g = game.getGraphics();
        g.drawPixmap(GraphicAssets.gameOver, 65, 50);
        g.drawPixmap(GraphicAssets.play, 320, 0);
        g.drawPixmap(GraphicAssets.quit, 0, 0);


        synchronized (touchEvents){
        for (Input.TouchEvent event : touchEvents){
            if (event.type == Input.TouchEvent.TOUCH_DOWN) {
                if(event.x > 320 && event.y < 140){
                    restartGame(environment.levelNumber);
                } else if(event.x < 160 && event.y < 140){
                    game.setScreen(new MenuScreen(game));
                }
            }
        }}
    }

    /**
     * Method which is called when the GameState is levelCompleted. This method listens for and checks all touch-
     * events in a synchronized block. This is so the data (List<touchEvents>) is not modified during iteration which
     * can cause a Concurrent Modification Exception. This method also displays the levelcompleted image and plays
     * won audio file.
     */
    private void completedUI(List<Input.TouchEvent> touchEvents){
        Graphics g = game.getGraphics();
        g.drawPixmap(GraphicAssets.complete, 65, 50);
        g.drawPixmap(GraphicAssets.play, 320, 0);
        g.drawPixmap(GraphicAssets.quit, 0, 0);

        if (!played) {
            GraphicAssets.won.play();
            played = true;
        }

        synchronized (touchEvents){
        for (Input.TouchEvent event : touchEvents){
            if (event.type == Input.TouchEvent.TOUCH_DOWN) {
                if(event.x > 320 && event.y < 140){
                    restartGame(environment.levelNumber + 1);
                } else if(event.x < 160 && event.y < 140){
                    game.setScreen(new MenuScreen(game));
                }
            }
        }}
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
