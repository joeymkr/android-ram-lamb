package com.emag.emag;

import android.graphics.Rect;

/**
 * Created by Joe Roberts on 27/04/2015.
 * Represents an entity within a 2d space. Is of type Collidable the obstacle can be interacted with by other obstacles.
 * Holds attributes to represent its 2d qualities such as position with x and y, and its width and height. Implements 
 * buildBounds from Collidable, which takes this obstacles bounds and creates four Rects that represent each side of the 
 * obstacle. This is then used to determine which side of the obstacle has been collided with.
 */
public abstract class Obstacle implements Collidable {
    private Rect[] bounds =  new Rect[4];
    private int x;
    private int y;
    private int width;
    private int height;

    /**
     * Constructs a 2D representation of an obstacle in 2D space
     * @param x used as a starting position on the screen
     * @param y used as a starting position on the screen
     * @param width total width of obstacle
     * @param height total height of obstacle
     */
    public Obstacle(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    /**
     * Creates four sides surrounding the bounds of this platform to form a surrounding square. These four sides are 
     * then iterated through to check which side if any, have been intersected. Uses a Rect object to achieve this.
     * @param x x position in 2D space of opposing obstacle
     * @param y y position in 2D space of opposing obstacle
     * @param width width of opposing obstacle
     * @param height height of opposing obstacle
     * @return array of four Rects representing a square the makes up the four sides of this obstacle.
     */
    @Override
    public Rect[] buildBounds(int x, int y, int width, int height){
        bounds[0] = new Rect((x+5), y, ((x+width)-10),(y-5)); //top of platform
        bounds[1] = new Rect((x+width), (y+1), (x+5),(y+height-2)); //right side of platform
        bounds[2] = new Rect((x+3), (y+height), ((x+width)-10),(y+height-5)); //bottom of platform
        bounds[3] = new Rect(x, (y+5), (x-1),(y+height-5)); //left side of platform
        return bounds;
    }

    public void moveObstacleHorizontally(int dir){
        x += dir;
    }


    public int getX(){
        return x;
    }

    public int getY(){
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    /**
     * Method to get the position of the side that has been collided with to apply opposing force on colliding obstacle
     * @param boundary
     * @return
     */
    public Position getBoundIntersection(int boundary){
        int boundX = 0;
        int boundY = 0;
        switch (boundary){
            case 0:
                boundX = x;
                boundY = y;
                break;
            case 1:
                boundX = x+width;
                boundY = y+1;
                break;
            case 2:
                boundX = x;
                boundY = y+height;
                break;
            case 3:
                boundX = x;
                boundY = y+1;
                break;
            default:
                break;
        }
        return new Position(boundX,boundY);
    }

    public abstract int checkCollision(Rect object);
}
