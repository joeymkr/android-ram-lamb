package com.emag.emag;

import android.graphics.Rect;

/**
 * Created by Joe Roberts on 26/04/2015.
 * Represents a platform depicted by a ground image within the game. Extends obstacle as the platform needs to
 * be able to collide with the player.
 */
public class Platform extends Obstacle{


    /**
     * Construtor. Takes initialisers for x and y starting position on screen and width and height
     * @param x
     * @param y
     */
    public Platform (int x, int y){
        super(x, y, 200, 25);
    }

    /**
     * This class is a type of collidable. Inherits method from Obstacle which creates four sides surrounding the
     * bounds of this platform to form a surrounding square. These four sides are then iterated through to check which
     * side if any, have been intersected. Uses a Rect object to achieve this.
     * @param object parameter given in this case is the players bounds
     * @return returns the Rect array index to be used to isolate the side of this object that has been intersected.
     * Returns 0-3 if a side has been intersected starting from top - right - bottom - left, returns -1 is none have.
     */
    public int checkCollision(Rect object){
        Rect[] bounds = buildBounds(getX(),getY(),getWidth(),getHeight());
        for (int i = 0; i < 4; i++){
            if(Rect.intersects(object,bounds[i])){
                return i;
            }
        }

        return -1;
    }


}
