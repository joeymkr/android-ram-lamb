package com.emag.emag;

import android.graphics.Rect;

/**
 * Created by Joe Roberts on 25/04/2015.
 * Holds all the variables and logic to deal with the environment and obstacles within the environment. Determines laws
 * of physics and collision detection rules. Builds each environment or level based on the levelNumber variable which is
 * either 1, 2 or 3. initialises the wolves, platforms, sheep and target using hard coded starting position variables.
 */
public class Environment {
    private int bkX = 0;
    private int bkY = 0;
    private int groundX = 0;
    private int groundY = 270;

    public int levelNumber;

    public Wolf[] wolves;

    public Platform[] platforms;

    //starting positions for the platforms in each level
    private Position[][] platformPos = {new Position[]{
                new Position(300,200), new Position(600,120), new Position(800,120), new Position(1000,80), new Position(1200,200), new Position(1400,100)},
            new Position[]{
                new Position(300,230), new Position(500,180), new Position(700,130), new Position(1100,130), new Position(1300,200), new Position(1500,150), new Position(1700,150), new Position(1900,90)},
            new Position[]{
                new Position(300,230), new Position(500,180), new Position(700,130), new Position(1100,130), new Position(1300,240), new Position(1500,200), new Position(1700,150), new Position(1900,90)}
    };

    //starting positions for the wolves in each level
    private Position[][] wolvesPos = {new Position[]{
                new Position(300,200), new Position(1150,80), new Position(1200,200), new Position(1550,100), new Position(1700,270)},
            new Position[]{
                    new Position(500,180), new Position(1100,270), new Position(1300,200), new Position(1650,150), new Position(1700,270)},
            new Position[]{
                    new Position(500,180), new Position(1100,270), new Position(1100,130), new Position(1450,240), new Position(1700,150), new Position(1800,270), new Position(1900,90), new Position(2000,270)}
    };

    //target positions in each level
    private Position[] targetPos = {new Position(1800, 248),
            new Position(2000,65), new Position(1800,248)
            };

    public Sheep player;
    public Sheep target;
    private GameScreen game;


    /**
     * Constructors needs a game for super and a initial level number to run. Constructor creates instances of sheep and
     * target and calls methods to build the platforms and wolves.
     * @param game
     * @param levelNumber
     */
    public Environment(GameScreen game, int levelNumber){
        this.levelNumber = levelNumber;
        this.game = game;
        player = new Sheep();
        target = new Sheep(targetPos[levelNumber-1].x, targetPos[levelNumber-1].y, 25, 25);
        buildPlatforms();
        buildWolves();
    }

    /**
     * creates new instance of platforms, iterates through then and instantiates them with the positions[] coordinates
     * as their starting positions on screen.
     */
    private void buildPlatforms(){

        switch (levelNumber){
            case 1:
                platforms = new Platform[6];
                break;
            case 2:
                platforms = new Platform[8];
                break;
            case 3:
                platforms = new Platform[8];
        }
        for (int i = 0; i < platforms.length; i++){
            platforms[i] = new Platform(platformPos[levelNumber - 1][i].x, platformPos[levelNumber-1][i].y);
        }
    }

    /**
     * creates new instance of wolves, iterates through then and instantiates them with the positions[] coordinates
     * as their starting positions on screen.
     */
    private void buildWolves(){
        switch (levelNumber){
            case 1:
                wolves = new Wolf[5];
                break;
            case 2:
                wolves = new Wolf[5];
                break;
            case 3:
                wolves = new Wolf[8];
        }
        Direction dir = Direction.RIGHT;
        for (int i = 0; i < wolves.length; i++) {
            wolves[i] = new Wolf(wolvesPos[levelNumber-1][i].x, wolvesPos[levelNumber-1][i].y, dir, 20);
            if (dir == Direction.RIGHT){
                dir = Direction.LEFT;
            } else {
                dir = Direction.RIGHT;
            }
        }
    }

    /**
     * Checks background position and resets position for continuous loop, also includes the ground. Checks collisions
     * will all visible platforms and wolves. If a platform has collided with the player, the player is position adjacent
     * to the side that it has intersected.
     * 
     * If a wolf has been collided with then we check if the player was attacking and if so what direction were both the 
     * player and wolf, this is to determine whether the player gets hurt or the wolf dies. This method has overriding 
     * privileges for all obstacles positions and states within the environment.
     */
    public void updateEnvironment(){
        //resets background image for continuous loop
        if (bkX <= (0-602)){
            bkX = 0;
        }
        //resets ground image for continuous loop
        if (groundX <= (0-601)){
            groundX = 0;
        }

        int plX = player.getX()+10;
        int plY = player.getY();

        //checks platform collisions
        for (Platform platform : platforms){

            if (platform.getX() < 480 && platform.getX() > -200){
                int collision = platform.checkCollision(new Rect(plX, plY, plX+31, plY+46));

                if (collision > -1){
                    setPlayerCollisionPosition(collision, platform);
                }
            }
        }

        //checks wolf collisions
        for (Wolf wolf: wolves){
            //check if wolf is visible
            if (wolf.getX() < 480 && wolf.getX() > -200){
                //check wolf is alive
                if (wolf.isCharAlive()){
                    int collision = wolf.checkCollision(new Rect(plX, plY, plX+31, plY+46));
                    //check if wolf and player have collided
                    if(collision > -1){
                        //check if player is attacking
                        if(player.isAttacking()){
                            //check if player attacked from the correct side
                            boolean hasHurtWolf = checkAttackingDirection(wolf);
                            if (hasHurtWolf){
                                wolf.kill();
                            } else {
                                if(!player.isHurt()) {
                                    hurtPlayer();
                                }
                            }
                        } else {
                            if(!player.isHurt()) {
                                hurtPlayer();
                            }
                        }
                    }
                }
            }
        }

        if (target.getX() < 480 && target.getX() > -50){
            int collision = target.checkCollision(new Rect(plX, plY, plX+31, plY+46));

            if (collision > -1){
                game.setGameState(GameScreen.GameState.LevelComplete);
            }
        }

        if(player.getY() > 230){
            player.setY(230);
        }

        if (player.getX() > 300){
            player.setX(300);
        }

        if (player.getX() < 5){
            player.setX(5);
        }

        if (player.getX() < 0){
            player.kill();
        }
    }

    private void hurtPlayer(){
        player.reduceLife();
        //move player back
        if (player.getFacingDirection() == Direction.LEFT) {
            player.moveObstacleHorizontally(10);
        }else {
            player.moveObstacleHorizontally(-10);
        }
        player.setHurting();
    }

    private boolean checkAttackingDirection(Wolf wolf){
        if (player.getFacingDirection() == wolf.getFacingDirection()){
            return true;
        }
        return false;
    }

    private void setPlayerCollisionPosition(int collision, Platform platform){
        int newY;
        int newX;
        switch(collision) {
            case 0:
                newY = (platform.getBoundIntersection(collision).y - 46);//gets y of platform top and adds sheep sprite height
                player.setY(newY);
                break;
            case 1:
                newX = (platform.getBoundIntersection(collision).x);//gets x of platform right
                player.setX(newX);
                break;
            case 2:
                newY = (platform.getBoundIntersection(collision).y);//gets y of platform bottom
                player.setY(newY);
                break;
            case 3:
                newX = (platform.getBoundIntersection(collision).x - 61);//gets x of platform right and minuses sheep sprite width
                player.setX(newX+10);
                break;
        }
    }

    /**
     * Applies the law of gravity to the player.
     */
    public void gravity(){
        if(player.getY() < 230 && !player.isJumping()){
            player.moveCharacterDown(3);
        }
    }

    public int getBkX() {
        return bkX;
    }

    public int getBkY(){
        return bkY;
    }

    public int getGroundX (){
        return groundX;
    }

    public int getGroundY (){
        return groundY;
    }

    /**
     * moves all obstacles in the environment left apart from the player
     */
    public void moveEnviroLeft(){
        bkX -= 1;
        groundX -=2;
        int speed = 2;
        if (player.getX() > 270){speed = 3;}
        target.moveCharacterLeft(speed);
        moveObstacles(platforms, -speed);
        moveObstacles(wolves,-speed);
    }

    /**
     * moves all obstacles in the environment right apart from the player
     */
    public void moveEnviroRight(){
        bkX += 1;
        groundX +=2;
        target.moveCharacterRight(2);
        moveObstacles(platforms, 2);
        moveObstacles(wolves,2);
    }

    private void moveObstacles(Obstacle[] obstacles, int dir){
        for (Obstacle obstacle : obstacles){
            obstacle.moveObstacleHorizontally(dir);
        }
    }
}
