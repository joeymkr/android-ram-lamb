package com.emag.emag;

/**
 * Created by Joe Roberts on 26/04/2015.
 * Holds x and y position
 */
public class Position {
    public int x;
    public int y;

    public Position(int x, int y){
        this.x = x;
        this.y = y;
    }
}
