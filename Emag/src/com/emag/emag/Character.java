package com.emag.emag;


/**
 * Created by Joe Roberts on 25/04/2015.
 * Holds character attributes and methods to animate this character. Attributes include facing direction variable, 
 * is alive boolean and current sprite number used for determining the current sprite. Also holds timers for animation
 * and methods to set animation and change sprites. Also has the ability to kill this character.
 */
public abstract class Character extends Obstacle{
    private boolean isAlive = true;
    protected Direction facing = Direction.RIGHT;
    private int timer;
    private int spriteNumber;
    private int animationSpeed = 5;

    /**
     * requires x and y starting positions for on screen along with width and height of character.
     * @param x starting x position on screen
     * @param y starting y position on screen
     * @param width total width of character
     * @param height total height of character
     */
    public Character (int x, int y, int width, int height){
        super(x, y, width,height);
        spriteNumber = 0;
        timer = animationSpeed;
    }

    /**
     * sets the frame rate of animation. used with timer
     * @param speed
     */
    public void setAnimationSpeed(int speed){
        animationSpeed = speed;
    }

    public int getSpriteNumber(){
        return spriteNumber;
    }

    public void setFacingDir(Direction facing){
        this.facing = facing;
        if (facing == Direction.RIGHT){
            spriteNumber = 0;
        }else {
            spriteNumber = 3;
        }
    }

    public Direction getFacingDirection(){
        return facing;
    }

    /**
     * kills this character. sets isAlive flag
     */
    public void kill(){
        isAlive = false;
    }

    public boolean isCharAlive(){
        return isAlive;
    }

    public abstract void updateCharacter ();

    /**
     * Decrements a timer by 1 each iteration. when the timer reached the animation speed (frame rate) the sprite changes
     * when the sprite index reaches the limit of 3 or 6 it is reset.
     */
    public void animateCharacter(){
        timer -= 1;
        if (timer <= 0) {
            spriteNumber += 1;
            timer = animationSpeed;
        }
        if (facing == Direction.RIGHT) {
            if (spriteNumber > 2) {
                spriteNumber = 0;
            }
        }else {
            if (spriteNumber > 5) {
                spriteNumber = 3;
            }
        }

    }

    public void moveCharacterLeft(int speed){
        setX(getX()-speed);
    }

    public void moveCharacterRight(int speed){
        setX(getX() + speed);
    }

    public void moveCharacterUp(int speed){
        setY(getY() - speed);
    }

    public void moveCharacterDown(int speed){

        setY(getY() + speed);
    }
}
