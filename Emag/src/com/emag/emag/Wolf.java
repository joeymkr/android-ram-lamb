package com.emag.emag;

import android.graphics.Rect;

/**
 * Created by Joe Roberts on 27/04/2015.
 * Stores wolf base position and facing state. Is a type of Character implements updateCharacter in which
 * animation of the wolf character takes place. Here the facing state variable and movement direction are set, the facing
 * variable being used to decide character sprite.
 */
public class Wolf extends Character {
    private int posInc = 0;
    private int baseX = 0;

    /**
     * Constructor to initialise default x and y position on screen. Also gives default facing direction of enum
     * Direction.RIGHT and animation speed of 100.
     */
    public Wolf(){
        this(0, 230, Direction.RIGHT, 100);
    }

    /**
     * Constructor to initialise x and y position on screen as well as facing direction and animation speed.
     * @param x starting x position on screen
     * @param y starting y position on screen
     * @param direction starting facing position
     * @param animateSpeed used in Character.animateCharacter() to initiate animation timing
     */
    public Wolf(int x, int y, Direction direction, int animateSpeed){
        super(x, y-50, 50, 50);
        setAnimationSpeed(animateSpeed);
        facing = direction;
        if (facing == Direction.LEFT){
            posInc = 150;
        } else{
            posInc = 0;
        }
        baseX = x;
    }

    /**
     * Adjusts wolfs x position and direction it is facing using a timer. The wolf has a space of 150 pixels to move,
     * when that limit is reached it is then decremented back to 0. Uses a variable to hold base starting position
     * so that during environment movement due to user interaction, the wolfs movements are fluid and abide by laws of
     * movement and direction.
     */
    @Override
    public void updateCharacter() {
        //gets the base starting position
        baseX = getX() - posInc;
        if(facing == Direction.RIGHT){

            posInc += 1;
            if (posInc > 150){
                facing = Direction.LEFT;
            }

        } else if(facing == Direction.LEFT){

            posInc -= 1;
            if (posInc < 0){
                facing = Direction.RIGHT;
            }
        }
        //sets new position based on animation frame
        setX(baseX + posInc);
        animateCharacter();
    }

    /**
     * This class is a type of collidable. Inherits method from Obstacle which creates four sides surrounding the
     * bounds of this wolf to form a square. These four sides are then iterated through to check which side if any,
     * have been intersected. Uses a Rect object to achieve this.
     * @param object parameter given in this case is the players bounds
     * @return returns the Rect array index to be used to isolate the side of this object that has been intersected.
     * Returns 0-3 if a side has been intersected, returns -1 is none have.
     */
    public int checkCollision(Rect object){
        Rect[] bounds = buildBounds(getX(),getY(),getWidth(),getHeight());
        for (int i = 0; i < 4; i++){
            if(Rect.intersects(object,bounds[i])){
                return i;
            }
        }

        return -1;
    }
}
