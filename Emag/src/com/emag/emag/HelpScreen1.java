package com.emag.emag;

import com.emag.gameframework.Game;
import com.emag.gameframework.Graphics;
import com.emag.gameframework.Input;
import com.emag.gameframework.Screen;

import java.util.ConcurrentModificationException;
import java.util.List;

/**
 * Created by Joe Roberts on 28/04/2015.
 * Type of screen which displays help page 1 image and listens for and deals with help screen touch events. The touch events
 * are iterated through in a synchronized block to determine whether a set of specific areas of the screen have been
 * touched. The synchronization is so that the iterated data (list of touchEvents) is not modified during iteration which
 * can cause a Concurrent Modification Exception.
 */
public class HelpScreen1 extends Screen {
    public HelpScreen1(Game game) {
        super(game);
    }

    /**
     * Method which is called from AndroidFastRenderView using a timer. This method listens for and checks all touch-
     * events in a synchronized block. This is so the data (List<touchEvents>) is not modified during iteration which
     * can cause a Concurrent Modification Exception.
     * @param deltaTime
     */
    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();

        try{
        synchronized (touchEvents){
        for (Input.TouchEvent event : touchEvents){
            if (event.type == Input.TouchEvent.TOUCH_DOWN) {
                if(event.x > 400 && event.y > 250){
                    game.setScreen(new HelpScreen2(game));
                }
            }
        }}}catch (ConcurrentModificationException cme){

        }
    }

    @Override
    public void present(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(GraphicAssets.help1, 0, 0);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
