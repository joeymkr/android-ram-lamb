package com.emag.emag;

import com.emag.gameframework.Game;
import com.emag.gameframework.Graphics;
import com.emag.gameframework.Screen;

/**
 * Created by Joe Roberts on 24/04/2015.
 * Type of Screen. On update it loads all assets into memory. These assets include all images and sound files needed
 * to display and run the game. Once the assets are loaded, the MenuScreen is called.
 */
public class LoadingScreen extends Screen {
    public LoadingScreen(Game game){
        super(game);
    }
    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        //background
        GraphicAssets.menu = g.newPixmap("menu.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.background[0] = g.newPixmap("background.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.background[1] = g.newPixmap("cave.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.background[2] = g.newPixmap("hell.png", Graphics.PixmapFormat.RGB565);

        //ground
        GraphicAssets.ground[0] = g.newPixmap("ground.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.ground[1] = g.newPixmap("ground2.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.ground[2] = g.newPixmap("ground3.png", Graphics.PixmapFormat.RGB565);

        //sheep images
        GraphicAssets.sheepImage[0] = g.newPixmap("char_r1.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.sheepImage[1] = g.newPixmap("char_r2.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.sheepImage[2] = g.newPixmap("char_r3.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.sheepImage[3] = g.newPixmap("char_r1left.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.sheepImage[4] = g.newPixmap("char_r2left.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.sheepImage[5] = g.newPixmap("char_r3left.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.sheepImage[6] = g.newPixmap("ramright.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.sheepImage[7] = g.newPixmap("ramleft.png", Graphics.PixmapFormat.RGB565);

        //wolf images
        GraphicAssets.wolfImage[0] = g.newPixmap("wolf1.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.wolfImage[1] = g.newPixmap("wolf2.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.wolfImage[2] = g.newPixmap("wolf3.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.wolfImage[3] = g.newPixmap("wolf4.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.wolfImage[4] = g.newPixmap("wolf5.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.wolfImage[5] = g.newPixmap("wolf6.png", Graphics.PixmapFormat.RGB565);

        //miscellaneous
        GraphicAssets.blank = g.newPixmap("blank.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.target = g.newPixmap("target.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.gameOver = g.newPixmap("gameover.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.complete = g.newPixmap("levelcompleted.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.info = g.newPixmap("info.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.numbers = g.newPixmap("numbers.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.help1 = g.newPixmap("help1.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.help2 = g.newPixmap("help2.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.play = g.newPixmap("play.png", Graphics.PixmapFormat.RGB565);
        GraphicAssets.quit = g.newPixmap("quit.png", Graphics.PixmapFormat.RGB565);

        GraphicAssets.won = game.getAudio().newMusic("won.mp3");

        game.setScreen(new MenuScreen(game));
    }

    @Override
    public void present(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
