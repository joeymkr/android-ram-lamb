package com.emag.emag;


import android.graphics.Rect;

/**
 * Created by Joe Roberts on 25/04/2015.
 * Stores Sheep attributes such as is sheep jumping, is sheep hurt, life, velocity and attack. Is a type of Character
 * implements updateCharacter in which animation of the sheep character takes place. Here the life of the sheep is checked
 * and is killed if life is 0. implements runnable which is used to animate and position the sheep during a jumping state.
 * The state of attack and hurt is also set within this class. The timers for both attacking and hurt are also set and
 * reset here.
 */
public class Sheep extends Character implements Runnable{
    private int velocity = 0;
    private boolean jumping = false;
    private boolean attacking = false;
    private int life = 100;
    private int hurtTimer = 100;
    private int attackingTimer = 50;
    private boolean hurt = false;

    /**
     * Constructor with default x and y on screen inititalisers
     */
    public Sheep(){
        this(0, 230);
    }
    /**
     * Constructor with default width and height inititalisers. Takes an x and y integer to set as x and y position
     * on screen
     */
    public Sheep(int x, int y){
        this(x, y, 51, 46);
    }

    /**
     * Sets the starting x and y positions on screen for this sheep. Also initialises the width and height.
     * @param x starting x position on screen
     * @param y starting y position on screen
     * @param width initial width of sheep
     * @param height initial height of sheep
     */
    public Sheep(int x, int y, int width, int height){
        super(x, y, width, height);
    }

    /**
     * Adjusts sheep's x position and direction it is facing using a timer during attacking state. Moves the sheep's y
     * position upward during a jumping state. Adjusts both hurt and attacking timers by decrementing them by 1 each
     * iteration. Then resets the timer once 0 is reached. This method also kills the sheep if life is 0 or less.
     */
    @Override
    public void updateCharacter (){
        if(life <= 0){
            kill();
            return;
        }
        if(jumping){
            moveCharacterUp(velocity);
        }
        if (hurt){
            hurtTimer -= 1;
            if (hurtTimer <= 0){
                hurt = false;
            }
        }

        if(attacking){
            attackingTimer -= 1;

            if (facing == Direction.LEFT){
                moveCharacterLeft(3);
            }else {
                moveCharacterRight(3);
            }

            if(attackingTimer <= 0){
                attacking = false;
            }
        }
    }

    public int getLife(){
        return life;
    }

    public void setHurting(){
        hurt = true;
        hurtTimer = 100;
    }

    public int getHurtTimer(){
        return hurtTimer;
    }

    public boolean isHurt(){
        return hurt;
    }

    public boolean isAttacking(){
        return attacking;
    }

    public void reduceLife(){
        life -= 20;
    }

    public boolean isJumping(){
        return jumping;
    }

    /**
     * When run, sets this sheep's jumping flag to true and sets the velocity of this sheep to 6. Uses four sleep
     * cycles adjusting the velocity between each. This gives a smooth jumping action. When the sheep starts the jump
     * it is at full velocity, after each sleep the velocity is decremented by 2 to slow the sheep down as the crest
     * of the jump is nearing.
     */
    @Override
    public void run() {
        velocity = 6;
        jumping = true;
        while (jumping){
            try {
                Thread.sleep(200);
                velocity = 4;
                Thread.sleep(200);
                velocity = 2;
                Thread.sleep(100);
                velocity = 0;
                Thread.sleep(100);
                jumping = false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This class is a type of collidable. Inherits method from Obstacle which creates four sides surrounding the
     * bounds of this sheep to form a square. These four sides are then iterated through to check which side if any,
     * have been intersected. Uses a Rect object to achieve this.
     * @param object parameter given in this case is the players bounds
     * @return returns the Rect array index to be used to isolate the side of this object that has been intersected.
     * Returns 0-3 if a side has been intersected, returns -1 is none have.
     */
    public int checkCollision(Rect object){
        Rect[] bounds = buildBounds(getX(),getY(),getWidth(),getHeight());
        for (int i = 0; i < 4; i++){
            if(Rect.intersects(object,bounds[i])){
                return i;
            }
        }

        return -1;
    }

    /**
     * sets sheep's attacking flag to true and sets the attacking timer to 50.
     */
    public void attack(){
        attacking = true;
        attackingTimer = 50;
    }
}
