package com.emag.emag;

/**
 * Created by Joe Roberts on 27/04/2015.
 * Used as a status for the direction of characters in a 2d space. Can either be facing left or right
 */
public enum Direction {
        LEFT,
        RIGHT
}
