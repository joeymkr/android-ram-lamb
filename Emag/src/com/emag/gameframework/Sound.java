package com.emag.gameframework;

public interface Sound {
	
	public void play(float volume);
	public void dispose();

}
