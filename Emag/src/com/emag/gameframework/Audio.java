package com.emag.gameframework;

public interface Audio {
	
	public Music newMusic(String fileName);
	
	public Sound newSound(String fileName);

}
